import dataclasses
import json
from pathlib import Path
from typing import Optional

from lxml import etree

SCRIPTS_DIR = Path(__file__).parent
SOURCE_DIR = SCRIPTS_DIR / "data"
TARGET_DIR = SCRIPTS_DIR.parent / "src" / "data"

XML_FILE_PATH = SOURCE_DIR / "XML, 2022" / "MeSH_DE_2022.xml"


@dataclasses.dataclass
class MeshDescriptorFlat:
    ui: str
    name_de: str
    name_en: str

    @staticmethod
    def from_merged(merged: "MeshDescriptorMerged"):
        return MeshDescriptorFlat(
            ui=merged.ui,
            name_de=merged.name_de,
            name_en=merged.name_en,
        )


@dataclasses.dataclass
class MeshDescriptorMerged(MeshDescriptorFlat):
    tree_paths: list[list[str]]


@dataclasses.dataclass
class MeshDescriptorTreeRoot:
    children: dict[str, "MeshDescriptorTreeNode"] = dataclasses.field(default_factory=dict)


@dataclasses.dataclass
class MeshDescriptorTreeNode(MeshDescriptorTreeRoot):
    ui: Optional[str] = None


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, MeshDescriptorTreeRoot):
            d = dataclasses.asdict(o)

            # Remove empty children in all nodes
            def remove_empty_children(node):
                for child in node["children"].values():
                    if not child["children"]:
                        del child["children"]
                    else:
                        remove_empty_children(child)

            remove_empty_children(d)

            return d

        elif dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)

        return super().default(o)


print("Reading XML file...")
tree = etree.parse(XML_FILE_PATH)
root = tree.getroot()


# Main categories, create manually
"""
Anatomy [A]
Organisms [B]
Diseases [C]
Chemicals and Drugs [D]
Analytical, Diagnostic and Therapeutic Techniques, and Equipment [E]
Psychiatry and Psychology [F]
Phenomena and Processes [G]
Disciplines and Occupations [H]
Anthropology, Education, Sociology, and Social Phenomena [I]
Technology, Industry, and Agriculture [J]
Humanities [K]
Information Science [L]
Named Groups [M]
Health Care [N]
Publication Characteristics [V]
Geographicals [Z]
"""

mesh_descriptors = [
    MeshDescriptorMerged(
        ui="A",
        name_de="Anatomie",
        name_en="Anatomy",
        tree_paths=[["A"]],
    ),
    MeshDescriptorMerged(
        ui="B",
        name_de="Organismen",
        name_en="Organisms",
        tree_paths=[["B"]],
    ),
    MeshDescriptorMerged(
        ui="C",
        name_de="Krankheiten",
        name_en="Diseases",
        tree_paths=[["C"]],
    ),
    MeshDescriptorMerged(
        ui="D",
        name_de="Chemische und pharmazeutische Substanzen",
        name_en="Chemicals and Drugs",
        tree_paths=[["D"]],
    ),
    MeshDescriptorMerged(
        ui="E",
        name_de="Analytische, diagnostische und therapeutische Verfahren und Geräte",
        name_en="Analytical, Diagnostic and Therapeutic Techniques, and Equipment",
        tree_paths=[["E"]],
    ),
    MeshDescriptorMerged(
        ui="F",
        name_de="Psychiatrie und Psychologie",
        name_en="Psychiatry and Psychology",
        tree_paths=[["F"]],
    ),
    MeshDescriptorMerged(
        ui="G",
        name_de="Phänomene und Prozesse",
        name_en="Phenomena and Processes",
        tree_paths=[["G"]],
    ),
    MeshDescriptorMerged(
        ui="H",
        name_de="Disziplinen und Berufe",
        name_en="Disciplines and Occupations",
        tree_paths=[["H"]],
    ),
    MeshDescriptorMerged(
        ui="I",
        name_de="Anthropologie, Bildung, Soziologie und soziale Phänomene",
        name_en="Anthropology, Education, Sociology, and Social Phenomena",
        tree_paths=[["I"]],
    ),
    MeshDescriptorMerged(
        ui="J",
        name_de="Technik, Industrie und Landwirtschaft",
        name_en="Technology, Industry, and Agriculture",
        tree_paths=[["J"]],
    ),
    MeshDescriptorMerged(
        ui="K",
        name_de="Geisteswissenschaften",
        name_en="Humanities",
        tree_paths=[["K"]],
    ),
    MeshDescriptorMerged(
        ui="L",
        name_de="Informationswissenschaften",
        name_en="Information Science",
        tree_paths=[["L"]],
    ),
    MeshDescriptorMerged(
        ui="M",
        name_de="Benannte Gruppen",
        name_en="Named Groups",
        tree_paths=[["M"]],
    ),
    MeshDescriptorMerged(
        ui="N",
        name_de="Gesundheitswesen",
        name_en="Health Care",
        tree_paths=[["N"]],
    ),
    MeshDescriptorMerged(
        ui="V",
        name_de="Eigenschaften von Publikationen",
        name_en="Publication Characteristics",
        tree_paths=[["V"]],
    ),
    MeshDescriptorMerged(
        ui="Z",
        name_de="Geographische Bereiche",
        name_en="Geographicals",
        tree_paths=[["Z"]],
    ),
]

# Extract data from XML
print("Extracting data from XML...")
for descriptor_record in root.iterchildren():
    descriptor_name = descriptor_record.find("DescriptorName")
    descriptor_name_string = descriptor_name.find("String").text

    # descriptor_name_string format: "Name DE[Name EN]"
    descriptor_name_string_de = descriptor_name_string.split("[")[0]
    descriptor_name_string_en = descriptor_name_string.split("[")[1].split("]")[0]

    descriptor_ui = descriptor_record.find("DescriptorUI").text

    tree_numbers = []
    for tree_number in descriptor_record.iter("TreeNumber"):
        tree_number = tree_number.text.split(".")
        tree_number.insert(0, tree_number[0][0])
        tree_numbers.append(tree_number)

    mesh_descriptors.append(
        MeshDescriptorMerged(
            ui=descriptor_ui,
            name_de=descriptor_name_string_de,
            name_en=descriptor_name_string_en,
            tree_paths=tree_numbers,
        )
    )

# Not needed
# with open("mesh_descriptors.json", "w") as fp:
#     json.dump(mesh_descriptors, fp, indent=2, cls=CustomJSONEncoder)


# Convert to flat list and tree
print("Converting to flat list and tree...")

flat_nodes: dict[str, MeshDescriptorFlat] = {}
tree = MeshDescriptorTreeRoot()

for node in mesh_descriptors:
    # Add to flat list
    flat_node = MeshDescriptorFlat.from_merged(node)
    flat_nodes[flat_node.ui] = flat_node

    # Add to tree
    for tree_path in node.tree_paths:
        current_tree = tree

        for number in tree_path:
            if number not in current_tree.children:
                current_tree.children[number] = MeshDescriptorTreeNode()

            current_tree = current_tree.children[number]

        if not isinstance(current_tree, MeshDescriptorTreeNode):
            raise Exception("Tree path too short")

        if current_tree.ui is not None:
            raise Exception("Duplicate tree path")

        current_tree.ui = node.ui


with open(TARGET_DIR / "mesh_descriptors_flat.json", "w") as fp:
    json.dump(flat_nodes, fp, indent=2, cls=CustomJSONEncoder)

with open(TARGET_DIR / "mesh_descriptors_tree.json", "w") as fp:
    json.dump(tree, fp, indent=2, cls=CustomJSONEncoder)


def all_ancestors_of(tree: MeshDescriptorTreeRoot, selected: str) -> set[str]:
    result: set[str] = set()

    def traverse(node):
        nonlocal result
        if not node.children:
            return False

        child_is_selected = False
        for key, child in node.children.items():
            child_is_selected = traverse(child) or child_is_selected

            if type(node) is MeshDescriptorTreeRoot:
                continue

            if child_is_selected:
                result.add(node.ui)
            elif child.ui == selected:
                result.add(node.ui)
                child_is_selected = True

        return child_is_selected

    traverse(tree)
    return result

def all_descendants_of(tree: MeshDescriptorTreeRoot, selected: str) -> set[str]:
    result: set[str] = set()

    def traverse(node, is_selected):
        nonlocal result
        if is_selected and type(node) is not MeshDescriptorTreeRoot:
            result.add(node.ui)

        if not node.children:
            return

        for key, child in node.children.items():
            if is_selected:
                traverse(child, True)
            elif type(node) is not MeshDescriptorTreeRoot and node.ui == selected:
                traverse(child, True)
            else:
                traverse(child, False)

    traverse(tree, False)
    return result

# Precalculate all ancestors and descendants
print("Precalculating all ancestors and descendants...")

import multiprocessing

precalc_descendants: dict[str, str] = {}
precalc_ancestors: dict[str, str] = {}

def process_node(ui):
    ancestors = list(all_ancestors_of(tree, ui))
    descendants = list(all_descendants_of(tree, ui))
    return (ui, ancestors, descendants)

# Create a pool of workers
pool = multiprocessing.Pool()

uis = list(flat_nodes.keys())

# Process nodes in parallel and save results
results = pool.map(process_node, uis)
for node_ui, ancestors, descendants in results:
    precalc_ancestors[node_ui] = ancestors
    if descendants:
        precalc_descendants[node_ui] = descendants

# Save precalculated values to file
with open(TARGET_DIR / "mesh_descriptors_precalc.json", "w") as fp:
    json.dump({
        "descendants": precalc_descendants,
        "ancestors": precalc_ancestors,
    }, fp, indent=2, cls=CustomJSONEncoder)
