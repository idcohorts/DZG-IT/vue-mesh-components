#!/bin/sh
DATA_DIR="$(dirname "$0")/data"
mkdir -p ${DATA_DIR}

echo "Downloading MeSH descriptor files for 2022..."
# Source: https://repository.publisso.de/resource/frl%3A6432102
curl https://repository.publisso.de/resource/frl:6433710/data -o "${DATA_DIR}/xml_2022.zip"

echo "Unzipping..."
unzip -o "${DATA_DIR}/xml_2022.zip" -d "${DATA_DIR}"
