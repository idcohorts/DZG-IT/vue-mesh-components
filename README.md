# vue-mesh-components

This is a Vue 3 component library based on Oruga UI and Bulma that provides UI components for exploring, selecting and filtering by MeSH (Medical Subject Headings) terms. It has localization for English and German.

The MeSH data with German translations is provided by [PUBLISSO](https://repository.publisso.de/resource/frl%3A6432102).

## Installation

```bash
yarn add @osaris/vue-mesh-components
```

Ensure that the peer dependencies for
`@oruga-ui/oruga-next`,
`@oruga-ui/theme-bulma`,
and `vue`
are met.

## Usage

Currently, the main component provided by this package is the `MeSHFilter` component. It's designed to filter a list of medical studies that are tagged with MeSH terms. It provides a tree view of the MeSH terms, and an autocomplete input for searching for MeSH terms. It also provides a way to select MeSH terms directly, and to reset the filter.

```vue
<script setup lang="ts">
import { ref } from "vue";

import { MeSHFilter } from "@osaris/vue-mesh-components";

/*
Exposes the MeSHFilter component's API via the filter ref.
{
  clear: () => void,
  addByUi: (ui: MeSHUI) => void,
  removeByUi: (ui: MeSHUI) => void,
  meshDescriptorsSelectedDirectly: Ref<MeSHDescriptor[]>,
  meshDescriptorsSelectedTotal: Readonly<Ref<SelectedMeSHDescriptor[]>>,
  meshUIsSelectedTotal: Readonly<Ref<Set<MeSHUI>>>,
}
*/
const filter = ref<typeof MeSHFilter | null>(null);

const handleResetButtonClick = () => {
  filter.value?.clear();
};

// The MeSH UIs to restrict the tree/autocomplete to.
// Also used to determine counts in the tree.
const itemUIs = [
  ["D018516", "D028321", "D009858"],
  ["D000011", "D000406"],
  ["D020215", "D000406"],
];

const currentLanguage = ref<"en" | "de">("en");
</script>

<template>
  <button @click="handleResetButtonClick">Reset</button>

  <button @click="currentLanguage = currentLanguage === 'en' ? 'de' : 'en'">
    Toggle language ({{ currentLanguage }})
  </button>

  <MeSHFilter ref="filter" :lang="currentLanguage" :itemUIs="itemUIs" />
</template>
```

## Development

```bash
yarn
yarn dev
```

### Build

```bash
yarn build
```

### Publish

1. Ensure you are authenticated with npm by either running `yarn npm login --publish` or configuring your npm token in `.yarnrc.yml` or via the `NPM_TOKEN` environment variable.
2. Bump the version in `package.json`
3. Run the following commands:

```bash
yarn build
yarn npm publish
```

# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support For `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.
