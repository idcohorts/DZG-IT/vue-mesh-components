//import meshDescriptorsFlat from "@/data/mesh_descriptors_flat.json";
//export type MeSHUI = keyof typeof meshDescriptorsFlat;

export type MeSHUI = string;

export interface MeSHTreeNode {
  ui: MeSHUI;
  children?: Record<string, MeSHTreeNode>;
  uiCount?: number;
}

export interface MeSHTreeRoot {
  children: Record<string, MeSHTreeNode>;
  uiCount?: number;
}

export interface MeSHDescriptor {
  ui: MeSHUI;
  name_de: string;
  name_en: string;
}

export interface SelectedMeSHDescriptor extends MeSHDescriptor {
  isDescendant: boolean;
}
