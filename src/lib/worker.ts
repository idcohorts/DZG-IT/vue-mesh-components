import * as Comlink from "comlink";

import { autocomplete, prefilterTree } from "./algo";

const worker = {
  autocomplete,
  prefilterTree,
};

Comlink.expose(worker);

export type AlgoWorkerType = typeof worker;
