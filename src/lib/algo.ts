import { matchSorter } from "match-sorter";

import type {
  MeSHDescriptor,
  MeSHTreeNode,
  MeSHTreeRoot,
  MeSHUI,
} from "@/types/mesh";
import type { Language } from "@/types/lang";

export function prefilterTree(
  tree: MeSHTreeNode | MeSHTreeRoot,
  itemUIs: MeSHUI[][],
) {
  /**
   * Generates a tree that only contains nodes that occur in one of the items.
   * The tree is used to render the tree view.
   * Additionally, the resulting tree contains the number of items under that node.
   *
   * Also generates a set of all visible nodes in the tree.
   * This is used to filter the autocomplete results.
   */
  const visibleNodes = new Set<MeSHUI>();

  const uiValues = new Set(itemUIs.flat());

  const indexableItemUIs = Object.fromEntries(
    itemUIs.map((itemUI, index) => [index, itemUI]),
  );

  function traverse(tree: MeSHTreeNode | MeSHTreeRoot): {
    tree: MeSHTreeNode | MeSHTreeRoot | null;
    indicesChild: Set<string>;
  } {
    const newTree = { ...tree };
    newTree.children = {} as Record<string, MeSHTreeNode>;

    let contains = false;
    let indices = Object.entries(indexableItemUIs).reduce(
      (acc, [index, itemUI]) => {
        if ("ui" in tree && itemUI.some((i) => i === tree.ui)) acc.add(index);
        return acc;
      },
      new Set<string>(),
    );

    for (let key in tree.children) {
      const { tree: child, indicesChild } = traverse(tree.children[key]);

      if (child && "ui" in child) {
        newTree.children[key] = child;
        contains = true;
        visibleNodes.add(child.ui);
        for (let index of indicesChild) indices.add(index);
      } else if (
        tree.children &&
        tree.children[key].ui &&
        uiValues.has(tree.children[key].ui)
      ) {
        newTree.children[key] = {
          ui: tree.children[key].ui,
          uiCount: indicesChild.size,
        };
        contains = true;
        visibleNodes.add(tree.children[key].ui);
        for (let index of indicesChild) indices.add(index);
      }
    }

    newTree.uiCount = indices.size;

    return { tree: contains ? newTree : null, indicesChild: indices };
  }

  const traversalResult = traverse(tree) || { children: {}, uiCountChild: 0 };
  const filteredTree = traversalResult.tree;

  return {
    prefilteredTree: filteredTree as MeSHTreeRoot,
    visibleNodes,
  };
}

export function autocomplete(
  meshDescriptorList: MeSHDescriptor[],
  searchTerm: string,
  lang: Language,
  selected: MeSHUI[],
  infiniteScrollCounter: number,
) {
  if (searchTerm === "") return [];
  return matchSorter(meshDescriptorList, searchTerm, {
    keys: ["ui", `name_${lang}`],
  })
    .filter((item) => !selected.some((i) => i === item.ui))
    .slice(0, (infiniteScrollCounter + 1) * 100);
}

// Unused
export function hasSelectedDescendants(tree: MeSHTreeRoot, selected: MeSHUI[]) {
  const result = new Set<MeSHUI>();

  function traverse(node: MeSHTreeNode | MeSHTreeRoot): boolean {
    if (!node.children) return false;
    let childIsSelected = false;

    for (let key in node.children) {
      const child = node.children[key];
      childIsSelected = traverse(child) || childIsSelected;
      if (childIsSelected && "ui" in node) {
        result.add(node.ui);
      } else if (
        child.ui &&
        selected.some((i) => i === child.ui) &&
        "ui" in node
      ) {
        result.add(node.ui);
        childIsSelected = true;
      }
    }
    return childIsSelected;
  }

  traverse(tree);
  return result;
}

// Unused
export function hasSelectedAncestors(tree: MeSHTreeRoot, selected: MeSHUI[]) {
  const result = new Set<MeSHUI>();

  function traverse(node: MeSHTreeNode | MeSHTreeRoot, isSelected: boolean) {
    if (isSelected && "ui" in node) {
      result.add(node.ui);
    }

    if (!node.children) return;

    for (let key in node.children) {
      const child = node.children[key];
      if (isSelected) {
        traverse(child, true);
      } else if ("ui" in node && selected.some((i) => i === node.ui)) {
        traverse(child, true);
      } else {
        traverse(child, false);
      }
    }
  }

  traverse(tree, false);
  return result;
}
