import * as Comlink from "comlink";

import AlgoWorker from "@/lib/worker?worker&inline";
import { AlgoWorkerType } from "./worker";

export default () => {
  const worker = new AlgoWorker();
  const wrapped = Comlink.wrap<AlgoWorkerType>(worker);
  return {
    worker,
    wrapped,
  };
};
