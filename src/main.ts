import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

import Oruga from "@oruga-ui/oruga-next";
import { bulmaConfig } from "@oruga-ui/theme-bulma";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(fas);

import "bulma/css/bulma.css";
import "@oruga-ui/theme-bulma/dist/bulma.css";

const orugaConfig = {
  ...bulmaConfig,
  iconPack: "fas",
  iconComponent: "vue-fontawesome",
};

createApp(App)
  .component("VueFontawesome", FontAwesomeIcon)
  .use(Oruga, orugaConfig)
  .mount("#app");
