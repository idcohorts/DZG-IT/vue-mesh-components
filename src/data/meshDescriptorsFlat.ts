import type { MeSHDescriptor, MeSHUI } from "@/types/mesh";
import source from "./mesh_descriptors_flat.json";

const fixed = source as Record<MeSHUI, MeSHDescriptor>;

export default fixed;

export const meshDescriptorList = Object.values(fixed);
export const meshDescriptorsFlat = fixed;
