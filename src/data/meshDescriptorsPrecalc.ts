import type { MeSHUI } from "@/types/mesh";
import source from "./mesh_descriptors_precalc.json";

const fixed = source as {
  descendants: Record<MeSHUI, MeSHUI[]>;
  ancestors: Record<MeSHUI, MeSHUI[]>;
};

export const meshDescriptorsPrecalc = fixed;

export default fixed;
