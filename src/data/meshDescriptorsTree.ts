import type { MeSHTreeRoot } from "@/types/mesh";
import source from "./mesh_descriptors_tree.json";

const fixed = source as MeSHTreeRoot;

export const meshDescriptorsTree = fixed;

export default fixed;
