import type { App } from "vue";
import { MeSHFilter, MeSHAutocomplete, MeSHTree } from "@/components";

// Re-export types
export type {
  MeSHUI,
  MeSHDescriptor,
  MeSHTreeNode,
  MeSHTreeRoot,
} from "@/types/mesh";
export type { Language } from "@/types/lang";

// Re-export data
export {
  meshDescriptorList,
  meshDescriptorsFlat,
} from "@/data/meshDescriptorsFlat";
export { meshDescriptorsTree } from "@/data/meshDescriptorsTree";
export { meshDescriptorsPrecalc } from "@/data/meshDescriptorsPrecalc";

export default {
  install: (app: App) => {
    app.component("MeSHFilter", MeSHFilter);
    app.component("MeSHAutocomplete", MeSHAutocomplete);
    app.component("MeSHTree", MeSHTree);
  },
};

export { MeSHAutocomplete, MeSHFilter, MeSHTree };
