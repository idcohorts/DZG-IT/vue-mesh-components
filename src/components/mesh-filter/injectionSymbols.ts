import { InjectionKey, Ref } from "vue";

import type { MeSHDescriptor, SelectedMeSHDescriptor } from "@/types/mesh";
import type { Language } from "@/types/lang";

export const meshDescriptorsSelectedDirectlySymbol: InjectionKey<
  Ref<MeSHDescriptor[]>
> = Symbol("meshDescriptorsSelectedDirectly");

export const meshDescriptorsSelectedTotalSymbol: InjectionKey<
  Ref<SelectedMeSHDescriptor[]>
> = Symbol("meshDescriptorsSelectedTotal");

export const languageSymbol: InjectionKey<Ref<Language>> = Symbol("language");

export default {
  meshDescriptorsSelectedDirectly: meshDescriptorsSelectedDirectlySymbol,
  meshDescriptorsSelectedTotal: meshDescriptorsSelectedTotalSymbol,
  language: languageSymbol,
};
